#!/usr/bin/env bash

cd discovery-service
mvn clean install -Dmaven.test.skip=true
cd ../edge-service
mvn clean install -Dmaven.test.skip=true
cd ../monitoring-service/
mvn clean install -Dmaven.test.skip=true
cd ../quote-service/
mvn clean install -Dmaven.test.skip=true

docker-compose down
docker-compose up --force-recreate
