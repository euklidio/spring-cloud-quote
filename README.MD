### Quote of the day

Purpose of this application is to learn Spring Cloud Stack and microservices architecture

### Prerequisites
1. Java 8
2. GitBash or another bash emulation for windows
3. Docker 
4. Maven build tool

## Run application
Run `start.sh` 

The following components will be run under docker control:

- Discovery-Service (8761) - eureka - services registers here
- Edge-Service (8080) - acts as a gateway, balances load between two instances of quote service
- Monitoring-Service(8081) - check - https://github.com/codecentric/spring-boot-admin 
    integrated with discovery-service, read statuses from other services based on spring actuator 
- Comment(8082) - group and comment management server (group just contains list of comments, each comment need to belong to some group)
- Config (8888) - service for manage centralized configuration for all services. Configuration is controlled by git repository: https://mcreativo@bitbucket.org/euklidio/spring-cloud-comments-config.git
- Quote-Service  - two instances, there is no external access
- ELK stack - Elastic Search, Logstash, Kibana 
  - Logstash aggregates logs from services
  - Elastic search - indexes logs for purpose of quick searching
  - Kibana - Tool for visualization data 
  
### Troubleshouting 

For Windows 10, you can experience problem with docker compose and mounting volumes from host with relative paths.
If you can't see any logs in kibana, check logstash log: `docker logs logstash`
If logstash is exited because of error related to config file then the fix is following:

Run `docker cp ./logstash/logstash.conf logstash:/etc/logstash/conf.d/logstash.conf`
and then `docker start logstash`

## Stop application
Just run `stop.sh` to stop all services

## How to use

Go to `http://localhost:8761/` to show registered service, there should be registered three services
 * Edge service 
 * Monitoring service 
 * Quote Service - 2 instances
 
Go to `http://localhost:8081` to show spring boot admin panel when you can check status and details of registered services like
 * Memory usage
 * JVM info
 * Garbage collection info
 * Metrics like class loaded, threads, processors
 * Trace of traffic
 * Download heapdump

Go to `http://localhost:8080/quote` to see today's quote.

Go to Kibana panel `http://localhost:5601` to see logs of application.
There are many fields for example:
 * Logger name eg: `com.epam.quoteservice.controller.QuoteController`
 * Message eg `Retrieved quoted`
 * Instanced_id: one of two possibilities here: `INS1` or `INS2`
 
 
You can `http://localhost:8080/quote` couple times and see that load balancing is working and some
logs are from `INS1` and some from `INS2`
 

## Technologies used in project
 - Spring boot
 - Spring boot admin
 - Spring actuator
 - ElasticSearch
 - Logstash
 - Kiba
 - Eureka discovery service
 - Zuul
 - Ribbon
 - Docker
 - Docker compose
