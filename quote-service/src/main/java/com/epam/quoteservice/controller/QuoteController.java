package com.epam.quoteservice.controller;

import com.epam.quoteservice.entity.Quote;
import com.epam.quoteservice.repository.QuoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class QuoteController {

    @Autowired
    QuoteRepository repository;

    private final Logger LOGGER = LoggerFactory.getLogger(QuoteController.class);

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public Quote getQuote() {
        LOGGER.info("Retrieved quoted");
        return repository.getQuoteOfTheDay();
    }
}
