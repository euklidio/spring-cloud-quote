package com.epam.quoteservice.repository;

import com.epam.quoteservice.entity.Quote;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Repository
public class QuoteRepository {
    private static final Map<Integer, Quote> quotes;

    static {
        quotes = new HashMap<>();
        quotes.put(1, new Quote("We are what we repeatedly do. Excellence, therefore, is not an act but a habit.", "Aristotle"));
        quotes.put(2, new Quote("Whether you think you can or think you can’t, you’re right.", "Mark Twain"));
        quotes.put(3, new Quote("Success consists of doing the common things of life uncommonly well.", "Unknown"));
        quotes.put(4, new Quote("Remember that happiness is a way of travel, not a destination.", "Roy Goodman"));
        quotes.put(5, new Quote("The surest way not to fail is to determine to succeed.", "Richard B. Sheridan"));
        quotes.put(6, new Quote("Fall down seven times, get up eight.", "A Japanese Proverb"));
        quotes.put(7, new Quote("Sooner or later, those who win are those who think they can.", "Richard Bach"));
    }

    public Quote getQuoteOfTheDay() {
        return QuoteRepository.quotes.get(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }
}
