package com.epam.quoteservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@AllArgsConstructor
public class Quote {
    @NonNull
    private String content;
    @NonNull
    private String author;
}
